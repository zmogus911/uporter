import React, { Component } from 'react';

class Signup extends Component {
	render() {
		return (
			<div className="posts-list">
				<div className="container jumbotron">
					<div className="row">
						<div className="col-sm-6">
							<h1>Register</h1>
							<form>
								<div className="form-group">
									<label for="exampleFormControlInput1">Email address</label>
									<input
										required
										data-content="Must be a valid e-mail address (user@gmail.com)"
										type="email"
										className="form-control"
										id="exampleFormControlInput1"
										placeholder="name@example.com"
									/>
								</div>

								<div className="form-group">
									<label>Username</label>
									<input required type="text" className="form-control" placeholder="SuperBunny" />
								</div>

								<div className="form-group">
									<label>Enter password</label>
									<input required type="password" className="form-control" />
								</div>

								<div className="form-group">
									<label>Repeat password</label>
									<input required type="password" className="form-control" />
								</div>
								<button type="submit">Signup</button>
							</form>
						</div>

						<div className="col-sm-6">
							<h1>Login</h1>
							<form>
								<div className="form-group">
									<label for="exampleFormControlInput1">Email address</label>
									<input
										required
										type="email"
										className="form-control"
										id="exampleFormControlInput1"
										placeholder="name@example.com"
									/>
								</div>
								<div className="form-group">
									<label>Enter password</label>
									<input required type="password" className="form-control" />
								</div>
								<button type="submit">Login</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Signup;
