import React, { Component } from 'react';

class Categories extends Component {
	render() {
		return (
			<div className="categories">
				<button>All</button>
				<button>News</button>
				<button>Games</button>
				<button>Politics</button>
				<button>Jokes</button>
				<button>Music</button>
				<button>Travel</button>
			</div>
		);
	}
}

export default Categories;
