const Posts = require('./models/Post');
const User = require('./models/User');

module.exports = async function seed() {
	let count = await Posts.countDocuments().exec();
	if (!count) {
		await seedPostss();
	}

	count = await User.countDocuments().exec();
	if (!count) {
		await seedUsers();
	}
};

async function seedPostss() {
	return Posts.insertMany([
		{
			title: 'Testas 1',
			body: 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum'
		},
		{
			title: 'Testas 1',
			body: 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum'
		}
	]);
}

async function seedUsers() {
	const adminUser = new User({
		email: 'admin@localhost',
		username: 'admin'
	});

	adminUser.generateHash('admin1');

	return adminUser.save();
}
