import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from './../../logo.png';
import { slide as Menu } from 'react-burger-menu';

class Header extends Component {
	render() {
		return (
			<header className="header">
				<div className="header-inner">
					<Link to={'/'}>
						<img src={logo} className="logo" alt="logo" />
					</Link>
					<Link to={'/'}>
						<p className="brand-name">uporter</p>
					</Link>
					<Menu right isOpen={false}>
						<input className="search-input" placeholder="Search" />
						<Link to={'/signup'}>
							<p>SignIn</p>
						</Link>
						<Link to={'/profile'}>
							<p>Profile</p>
						</Link>
					</Menu>
				</div>
			</header>
		);
	}
}

export default Header;
