'use strict';

const router = require('express').Router();
const passport = require('passport');
const { asyncMiddleware: wrap } = require('../../helpers');

router.post(
	'/login',
	wrap(async (req, res, next) => {
		passport.authenticate('local', (err, user, msg) => {
			if (err) {
				return next(new Error(err));
			}
			if (!user) {
				return next(new Error(msg));
			}

			req.logIn(user, err => {
				if (err) {
					return next(new Error(err));
				}
				res.json({ status: 1 });
			});
		})(req, res, next);
	})
);

router.get('/logout', (req, res, next) => {
	if (req.isAuthenticated()) {
		req.logout();
	}

	res.send();
});

router.get('/isAuthenticated', (req, res, next) => {
	if (req.user) {
		return res.send(true);
	}

	return res.send(false);
});

module.exports = router;
