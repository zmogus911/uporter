'use strict';

const User = require('../../models/User');
const Validator = require('jsonschema').Validator;
const { validateId, asyncMiddleware: wrap } = require('../../helpers');
const router = require('express').Router();

const v = new Validator();

const validateJSON = (object, schema) => {
	var validationResult = v.validate(object, schema);
	if (validationResult.errors.length > 0) {
		var error = new Error('ValidationError');
		error.extra = validationResult.errors.join(', ');
		throw error;
	}
};

const createUserSchema = {
	type: 'object',
	properties: {
		email: { type: 'string', required: true, minLength: 3, maxLength: 32 },
		username: {
			type: 'string',
			required: true,
			minLength: 3,
			maxLength: 32
		},
		password: { type: 'string', required: true }
	},
	additionalProperties: false
};

router.delete(
	'/:id',
	wrap(async (req, res) => {
		await User.findOneAndRemove({ _id: req.params.id });
		res.end();
	})
);

router.put(
	'/:id',
	wrap(async (req, res, next) => {
		validateJSON(req.body, createUserSchema);
		const { email, username, password } = req.body;

		let predicate = { $or: [{ email }, { username }] };
		let existingUsers = await User.find(predicate);

		if (existingUsers.find(user => !user._id.equals(req.params.id))) {
			throw new Error('This user already exists');
		}

		const user = await User.findById(req.params.id);
		user.username = username;
		user.email = email;

		if (password) {
			user.generateHash(password);
		}

		await user.save();

		res.sendStatus(201);
	})
);

router.post(
	'/',
	wrap(async (req, res, next) => {
		validateJSON(req.body, createUserSchema);

		const { email, username, password } = req.body;

		let predicate = { $or: [{ email }, { username }] };

		let user = await User.findOne(predicate);
		if (user) {
			throw new Error('This user already exists');
		}

		user = await User.create({
			email,
			username
		});

		user.generateHash(password);
		await user.save();

		res.sendStatus(201);
	})
);

router.get(
	'/',
	wrap(async (req, res, next) => {
		let predicate = {};
		const { search, page, sort } = req.query;

		const limit = cap(req.query.limit | 0 || 1, 10, 100);

		if (search) {
			const regex = new RegExp(escapeRegExp(search), 'i');
			predicate.$or = [{ email: regex }, { username: regex }];
		}

		const fields = 'email username';
		const users = await User.find(predicate)
			.sort(sort)
			.limit(limit)
			.skip(limit * page - limit)
			.select(fields)
			.lean();
		const count = await User.count(predicate);

		res.json({ users, count });
	})
);

router.get(
	'/:id',
	wrap(async (req, res, next) => {
		validateId(req.params.id);
		const fields = 'email username';
		const users = await User.findById(req.params.id)
			.select(fields)
			.lean();

		res.json(users);
	})
);

const escapeRegExp = function(str) {
	return str.replace(/[-[\]/{}()*+?.\\^$|]/g, '\\$&');
};

const cap = function(n, min, max) {
	return Math.min(Math.max(n, min), max);
};

module.exports = router;
