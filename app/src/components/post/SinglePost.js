import React, { Component } from 'react';
import firstImage from './../../Images/1.jpg';
import secondImage from './../../Images/2.jpg';
import ninthImage from './../../Images/9.jpg';
class SinglePost extends Component {
	render() {
		return (
			<div className="single-post">
				<div className="post col-sm-12">
					<div id="carouselExampleIndicators" class="post-image carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img class="d-block w-100" src={firstImage} alt="First slide" />
							</div>
							<div class="carousel-item">
								<img class="d-block w-100" src={secondImage} alt="Second slide" />
							</div>
							<div class="carousel-item">
								<img class="d-block w-100" src={ninthImage} alt="Third slide" />
							</div>
						</div>
						<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true" />
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true" />
							<span class="sr-only">Next</span>
						</a>
					</div>
					<div className="post-content">
						<div className="post-hashtag">#politics #valdzia</div>
						<div className="post-title">Megathread:</div>
						<div>
							Michael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							RussiaMichael Cohen pleads guilty to lying to Congress about Trump real estate project in
							Russia
						</div>
						<div className="post-actions">
							<div>23 Comments</div>
							<div>44 Upvotes</div>
							<button className="post-upvote-button">up</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default SinglePost;
