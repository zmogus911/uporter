const mongoose = require('mongoose');

const schema = {
	title: String,
	body: String
};

module.exports = mongoose.model('Post', schema);
