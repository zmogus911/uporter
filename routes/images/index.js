'use strict';

const router = require('express').Router();
const Image = require('../../models/Image');
const { asyncMiddleware: wrap } = require('../../helpers');

const multer = require('multer');

const storage = multer.diskStorage({
	destination: 'uploads/',
	filename(req, file, cb) {
		cb(null, `${file.originalname}`);
	}
});

const upload = multer({ storage });

router.delete(
	'/:id',
	wrap(async (req, res) => {
		await Image.findOneAndRemove({ _id: req.params.id });
		res.end();
	})
);

router.post(
	'/',
	upload.single('file'),
	wrap(async (req, res) => {
		const image = await Image.create({
			path: '/uploads/' + req.file.filename
		});
		res.json(image);
	})
);

module.exports = router;
