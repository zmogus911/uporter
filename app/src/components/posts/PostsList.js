import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Categories from './Categories';
import firstImage from './../../Images/1.jpg';
import secondImage from './../../Images/2.jpg';
import ninthImage from './../../Images/9.jpg';

class PostsList extends Component {
	constructor() {
		super();
		this.state = {
			posts: [],
			error: '',
			spinner: ''
		};
	}

	render() {
		return (
			<div className="posts-list">
				<Categories />
				<h1>{this.state.posts.length}</h1>
				<div className="post row">
					<div className="col-6 col-sm-6">
						<img src={ninthImage} className="post-image" alt="logo" />
					</div>
					<div className="col-6 col-sm-6">
						<div className="post-hashtag">#politics #valdzia</div>
						<Link to={'/post'}>
							<div className="post-title">Megathread:</div>
						</Link>
						<p>Michael Cohen pleads guilty</p>
					</div>
					<div className="post-content col-12 col-sm-12">
						<div>
							<p>
								Michael Cohen pleads guilty to lying to Congress about Trump real estate project in
								Russia Michael Cohen pleads guilty to lying to Congress about Trump real estate project
								in Russia Michael Cohen pleads guilty to lying to Congress about Trump real estate
								project in Russia
							</p>
							<div className="post-actions">
								<div>23 Comments</div>
								<div>44 Upvotes</div>
								<button className="post-upvote-button">up</button>
							</div>
						</div>
					</div>
				</div>

				<div className="post row">
					<div className="col-6 col-sm-6">
						<img src={secondImage} className="post-image" alt="logo" />
					</div>
					<div className="col-6 col-sm-6">
						<div className="post-hashtag">#politics #valdzia</div>
						<Link to={'/post'}>
							<div className="post-title">Megathread:</div>
						</Link>{' '}
						<p>Michael Cohen pleads guilty</p>
					</div>
					<div className="post-content col-12 col-sm-12">
						<div>
							<p>
								Michael Cohen pleads guilty to lying to Congress about Trump real estate project in
								Russia Michael Cohen pleads guilty to lying to Congress about Trump real estate project
								in Russia Michael Cohen pleads guilty to lying to Congress about Trump real estate
								project in Russia
							</p>
							<div className="post-actions">
								<div>23 Comments</div>
								<div>44 Upvotes</div>
								<button className="post-upvote-button">up</button>
							</div>
						</div>
					</div>
				</div>

				<div className="post row">
					<div className="col-6 col-sm-6">
						<img src={firstImage} className="post-image" alt="logo" />
					</div>
					<div className="col-6 col-sm-6">
						<div className="post-hashtag">#politics #valdzia</div>
						<Link to={'/post'}>
							<div className="post-title">Megathread:</div>
						</Link>{' '}
						<p>Michael Cohen pleads guilty</p>
					</div>
					<div className="post-content col-12 col-sm-12">
						<div>
							<p>
								Michael Cohen pleads guilty to lying to Congress about Trump real estate project in
								Russia Michael Cohen pleads guilty to lying to Congress about Trump real estate project
								in Russia Michael Cohen pleads guilty to lying to Congress about Trump real estate
								project in Russia
							</p>
							<div className="post-actions">
								<div>23 Comments</div>
								<div>44 Upvotes</div>
								<button className="post-upvote-button">up</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	componentDidMount() {
		this.loadPosts();
	}

	loadPosts() {
		this.setState({
			spinner: true
		});
		fetch(`/api/posts`, {
			credentials: 'same-origin',
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(response => {
				if (!response.lots.length) {
					this.setState({
						error: 'No posts found.',
						spinner: false
					});
					return;
				}

				this.setState({
					posts: response.posts,
					error: '',
					spinner: false
				});
			})
			.catch(() => {
				this.setState({
					error: "Can't find any posts.",
					spinner: false
				});
			});
	}
}

export default PostsList;
