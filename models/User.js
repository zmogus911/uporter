const mongoose = require('mongoose');
const crypto = require('crypto');

const UserSchema = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	username: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	hash: { type: String },
	salt: { type: String }
});

UserSchema.methods.generateHash = function(password) {
	this.salt = crypto.randomBytes(16);
	this.hash = crypto
		.createHash('sha256')
		.update(Buffer.from(password + this.salt))
		.digest('hex');
};

UserSchema.methods.validPassword = function(password) {
	return (
		crypto
			.createHash('sha256')
			.update(Buffer.from(password + this.salt))
			.digest('hex') === this.hash
	);
};

UserSchema.methods.hasRight = function(right) {
	return this.rights.indexOf(right) !== -1;
};

const User = mongoose.model('User', UserSchema);
module.exports = User;
