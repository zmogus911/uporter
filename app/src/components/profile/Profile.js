import React, { Component } from 'react';
import secondImage from './../../Images/4.jpg';

class Signup extends Component {
	render() {
		return (
			<div className="posts-list">
				<div className="container jumbotron">
					<h1>Profile</h1>
					<div className="row">
						<div className="col-sm-3">
							<img src={secondImage} alt="..." className="img-thumbnail" />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<h5>Vardas: Giedrius</h5>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Signup;
