'use strict';

const Post = require('../../models/Post');
const Image = require('../../models/Image');

const { validateId, asyncMiddleware: wrap } = require('../../helpers');
const router = require('express').Router();

router.get(
	'/',
	wrap(async (req, res) => {
		console.log('loool');

		const fields = 'title body';
		const posts = await Post.find().select(fields);

		res.json(posts);
	})
);

router.get(
	'/:id',
	wrap(async (req, res) => {
		validateId(req.params.id);

		const fields = 'title body';
		const lot = await Post.findById(req.params.id)
			.select(fields)
			.lean();
		res.json(lot);
	})
);

const escapeRegExp = function(str) {
	return str.replace(/[-[\]/{}()*+?.\\^$|]/g, '\\$&');
};

const cap = function(n, min, max) {
	return Math.min(Math.max(n, min), max);
};

module.exports = router;
