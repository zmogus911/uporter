import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Header from './components/header/Header';
import PostsList from './components/posts/PostsList';
import SinglePost from './components/post/SinglePost';
import Signup from './components/signup/Signup';
import { AnimatedRoute } from 'react-router-transition';
import Profile from './components/profile/Profile';

import './styles/app.css';

class App extends Component {
	render() {
		return (
			<Router>
				<div className="App">
					<Header />
					<AnimatedRoute
						path="/"
						exact
						component={PostsList}
						runOnMount={true}
						atEnter={{ offset: -100 }}
						atLeave={{ offset: 0 }}
						atActive={{ offset: 0 }}
						mapStyles={styles => ({
							transform: `translateY(${styles.offset}%)`
						})}
					/>
					<AnimatedRoute
						path="/post"
						component={SinglePost}
						atEnter={{ offset: -50, opacity: 0 }}
						atLeave={{ offset: 0, opacity: 0 }}
						atActive={{ offset: 0, opacity: 1 }}
						mapStyles={styles => ({
							transform: `translateY(${styles.offset}%)`
						})}
					/>
					<AnimatedRoute
						path="/signup"
						component={Signup}
						atEnter={{ offset: -100 }}
						atLeave={{ offset: 0 }}
						atActive={{ offset: 0 }}
						mapStyles={styles => ({
							transform: `translateX(${styles.offset}%)`
						})}
					/>
					<AnimatedRoute
						path="/profile"
						component={Profile}
						atEnter={{ offset: -100 }}
						atLeave={{ offset: 0 }}
						atActive={{ offset: 0 }}
						mapStyles={styles => ({
							transform: `translateX(${styles.offset}%)`
						})}
					/>
				</div>
			</Router>
		);
	}
}

export default App;
