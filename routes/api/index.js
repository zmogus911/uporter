'use strict';

const mongooseConnection = require('../../bin/mongo-connect').connection;
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const log = require('../../helpers').log;
require('../../passport')(passport);

const router = require('express').Router();

router.use(
	session({
		store: new MongoStore({ mongooseConnection }),
		secret: 'DKwuZKKaEMdAV7Dp',
		resave: false,
		saveUninitialized: false,
		name: 'uporter.sid'
	})
);

router.use(passport.initialize());
router.use(passport.session());

// router.use('/auth', require('./auth'));
// router.use(requireAuthentication);
router.use('/posts', require('./posts'));
router.use('/users', require('./users'));

router.use((error, req, res, next) => {
	if (error.message === 'NotFound') {
		return res.status(404).json({ message: error.message });
	}

	if (error.message === 'This user already exists') {
		return res.status(400).json({ message: error.message });
	}

	if (error.message === 'Unauthorized') {
		return res.status(401).json({ message: error.message });
	}

	if (error.message === 'ValidationError') {
		res.status(400).json({ message: error.message, extra: error.extra });
		log('warn', error.message, { error, req, status: 400 });
		return;
	}

	res.status(500).json({ message: 'InternalServerError' });
	log('error', error.message, { error, req, data: req.body, status: 500 });
});

function requireAuthentication(req, res, next) {
	if (!req.isAuthenticated()) {
		return next(Error('Unauthorized'));
	}

	next();
}

module.exports = router;
